use std::cmp::Ordering;
use std::env;
use std::fmt::Display;
use std::fs;
use std::process;

type Weight = i64;
type WeightErr = u32;

struct WeightErrs {
    neg_err: WeightErr,
    pos_err: WeightErr,
}

impl WeightErrs {
    fn default() -> WeightErrs {
        WeightErrs {
            neg_err: WeightErr::MAX,
            pos_err: WeightErr::MAX,
        }
    }

    fn new_neg(neg_err: WeightErr) -> WeightErrs {
        debug_assert_ne!(neg_err, 0);

        WeightErrs {
            neg_err,
            pos_err: WeightErr::MAX,
        }
    }

    fn new_pos(pos_err: WeightErr) -> WeightErrs {
        debug_assert_ne!(pos_err, 0);

        WeightErrs {
            neg_err: WeightErr::MAX,
            pos_err,
        }
    }
}

impl Display for WeightErrs {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            if self.neg_err < self.pos_err {
                -(self.neg_err as i64)
            } else {
                self.pos_err as i64
            }
        )
    }
}

type WeightResult = Result<Vec<Weight>, WeightErrs>;

fn main() {
    const USAGE: &str = "Benutzung: a5 <eingabedatei>";

    let mut args = env::args();
    args.next();

    // Speichere den Dateipfad
    let path = args.next().unwrap_or_else(|| {
        // Beende das Programm, wenn kein Dateipfad angegeben wurde
        eprintln!("{}", USAGE);
        process::exit(1);
    });

    // Beende das Programm, wenn zu viele Argumente angegeben wurden
    if args.next().is_some() {
        eprintln!("{}", USAGE);
        process::exit(1);
    }

    // Lese die gegebene Datei ein
    let file = fs::read_to_string(path).expect("Die Datei konnte nicht geöffnet werden");

    // Wandle die Eingabedatei in eine Liste von Gewichten um
    let mut lines = file.lines();

    let mut weights = Vec::new();

    lines.next();
    for line in lines {
        let mut words = line.split_whitespace();

        let mass: u32 = words.next().unwrap().parse().unwrap();
        let count: usize = words.next().unwrap().parse().unwrap();

        for _ in 0..count {
            // Gewichte können auf beiden Seiten der Waage benuztzt werden
            weights.insert(0, -(mass as Weight));
            weights.push(mass as Weight);
        }
    }

    // Speichere den vorigen Error
    let mut prev_err: Option<WeightErrs> = None;

    // Iteriere über alle Zielgewichte
    'targs: for targ in (10..=10000).step_by(10) {
        // Überspringe das Zielgewicht, wenn der vorige Error positiv und > 10 ist
        if let Some(err) = &mut prev_err {
            if err.pos_err > 10 {
                // Aktualisiere den vorigen negativen Error
                err.neg_err += 10;

                // Aktualisiere den vorigen positiven Error
                if err.pos_err != WeightErr::MAX {
                    err.pos_err -= 10;
                }

                print_err(targ, err);

                continue;
            }
        }

        // 0-Sum
        let mut min_err = WeightErrs::new_neg(targ as u32);

        // 1-Sum
        match one_sum(&weights, targ) {
            Ok(sol) => {
                sort_print_sol(targ, sol);

                // Setze den vorigen Error zurück
                prev_err = None;

                continue 'targs;
            }
            Err(err) => upd_min_err(&mut min_err, err),
        }

        // n-Sum
        for n in 2..=weights.len() / 2 {
            match n_sum(&weights, targ, n) {
                Ok(sol) => {
                    sort_print_sol(targ, sol);

                    // Setze den vorigen Error zurück
                    prev_err = None;

                    continue 'targs;
                }
                Err(err) => upd_min_err(&mut min_err, err),
            }
        }

        print_err(targ, &min_err);

        // Aktualisiere den vorigen Error
        prev_err = Some(min_err);
    }
}

fn sort_print_sol(targ: Weight, mut sol: Vec<Weight>) {
    let len = sol.len();

    sol.sort_unstable();

    // Berechne die Anzahl der negativen Gewichte
    let mut neg_len = 0;
    for (i, weight) in sol.iter().enumerate() {
        if *weight > 0 {
            neg_len = i;
            break;
        }
    }

    print!("{}g: ", targ);

    // Gib alle negativen Gewichte aus
    #[allow(clippy::comparison_chain)]
    if neg_len == 1 {
        print!("{}g auf die linke Seite und ", -sol[0]);
    } else if neg_len > 1 {
        print!("{}g", -sol[0]);
        for weight in sol.iter().take(neg_len - 1).skip(1) {
            print!(", {}g", -weight);
        }
        print!(" und {}g auf die linke Seite und ", -sol[neg_len - 1]);
    }

    // Gib alle positiven Gewichte aus
    let pos_len = len - neg_len;
    if pos_len > 1 {
        print!("{}g", sol[neg_len]);
        for weight in sol.iter().take(len - 1).skip(neg_len + 1) {
            print!(", {}g", weight);
        }
        print!(" und ");
    }
    println!("{}g auf die rechte Seite der Waage stellen.", sol[len - 1]);
}

fn print_err(targ: Weight, err: &WeightErrs) {
    println!(
        "{}g: Kann mit den gegebenen Gewichten nur mit einem Error von {}g gemessen werden.",
        targ, err
    );
}

fn upd_min_err(min_err: &mut WeightErrs, err: WeightErrs) {
    // Aktualisiere den minimalen negativen Error
    if err.neg_err < min_err.neg_err {
        min_err.neg_err = err.neg_err;
    }

    // Aktualisiere den minimalen positiven Error
    if err.pos_err < min_err.pos_err {
        min_err.pos_err = err.pos_err;
    }
}

fn one_sum(weights: &[Weight], targ: Weight) -> WeightResult {
    let len = weights.len();

    debug_assert!(len >= 2);

    // Prüfe, ob das Zielgewicht zu klein ist
    if targ < weights[0] {
        return Err(WeightErrs::new_pos((weights[0] - targ) as WeightErr));
    }

    // Prüfe, ob das Zielgewicht zu groß ist
    if targ > weights[len - 1] {
        return Err(WeightErrs::new_neg((targ - weights[len - 1]) as WeightErr));
    }

    let mut min_err = WeightErrs::default();

    // Suche mit Binary-Search nach dem Zielgewicht
    let mut low = 0;
    let mut high = len - 1;
    while low <= high {
        let mid = low + (high - low) / 2;
        let weight = weights[mid];

        let err = match weight.cmp(&targ) {
            Ordering::Less => {
                low = mid + 1;
                WeightErrs::new_neg((targ - weight) as WeightErr)
            }
            Ordering::Greater => {
                high = mid - 1;
                WeightErrs::new_pos((weight - targ) as WeightErr)
            }
            Ordering::Equal => return Ok(vec![targ]),
        };

        upd_min_err(&mut min_err, err);
    }

    Err(min_err)
}

fn two_sum(weights: &[Weight], targ: Weight) -> WeightResult {
    let len = weights.len();

    debug_assert!(len > 2);
    debug_assert_ne!(targ, 0);

    let mut min_err = WeightErrs::default();

    let mut low = 0;
    let mut high = len - 1;

    while low < high {
        let sum = weights[low] + weights[high];

        // Prüfe, ob die Summe gleich dem Zielgewicht ist
        if sum == targ {
            return Ok(vec![weights[low], weights[high]]);
        }

        if weights[low] != -weights[high] {
            let err = if sum < targ {
                WeightErrs::new_neg((targ - sum) as WeightErr)
            } else {
                WeightErrs::new_pos((sum - targ) as WeightErr)
            };

            upd_min_err(&mut min_err, err);
        }

        // Vergleiche die Summe und das Zielgewicht
        if sum < targ {
            low += 1;
        } else {
            high -= 1;
        }
    }

    Err(min_err)
}

fn n_sum(weights: &[Weight], targ: Weight, n: usize) -> WeightResult {
    let len = weights.len();

    debug_assert!(n > 1);
    debug_assert!(n <= len / 2);

    // Prüfe, ob das Zielgewicht zu klein ist
    let smallest_sum = weights[..n].iter().sum();
    if targ < smallest_sum {
        return Err(WeightErrs::new_pos((smallest_sum - targ) as WeightErr));
    }

    // Prüfe, ob das Zielgewicht zu groß ist
    let largest_sum = weights[len - n..].iter().sum();
    if targ > largest_sum {
        return Err(WeightErrs::new_neg((targ - largest_sum) as WeightErr));
    }

    if n == 2 {
        return two_sum(weights, targ);
    }

    let mut min_err = WeightErrs::default();

    for i in 0..=1 {
        let mut prev = 0;

        for j in 0..=len / 2 - n {
            let curr = weights[if i == 0 { len - 1 - j } else { j }];

            // Überspringe Gewichte, die mehrmals vorkommen
            if prev == curr {
                continue;
            }
            prev = curr;

            match n_sum(&weights[j + 1..len - 1 - j], targ - curr, n - 1) {
                Ok(mut sol) => {
                    sol.push(curr);
                    return Ok(sol);
                }
                Err(err) => upd_min_err(&mut min_err, err),
            }
        }
    }

    Err(min_err)
}
